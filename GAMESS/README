GAMESS often requires careful management of memory. Quantum chemistry in particular is memory bound.

In the $SYSTEM group of the input file, you may need to set MWORDS and MEDDI. 

MWORDS is the maximum replicated memory which a job can use, on every core. This is given in units of 1,000,000 words (a word is 64 bits, 8 bytes)

MEMDDI is the total memory needed for the distributed data interface (DDI) storage, given in units of 1,000,000 words. The memory required on each processor core for a run using n cores is MEMDDI/n + MWORDS.

See:
http://www.msg.ameslab.gov/gamess/GAMESS_Manual/input.pdf
