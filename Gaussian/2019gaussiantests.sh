#!/bin/bash
# This script generates slurm scripts for the standard Gaussian tests.
# To submit the jobs use the following loop:
# for test in {0001..1044}; do sbatch job${test}.slurm; done
# Enjoy submitting 1044 Gaussian test jobs!
# Lev Lafayette, 2017
# Updated with new build system, 2020, LL
for test in {0001..1044}
do
cat <<- EOF > job${test}.slurm
#!/bin/bash
#SBATCH --job-name="Gaussian Test ${test}"
#SBATCH --ntasks=1
#SBATCH --time=12:00:00
module purge
module load pgi/18.10-gcc-8.3.0-2.32
module load gaussian/g16c01
g16 < test${test}.com > test${test}.log
EOF
done
