#!/bin/bash
#SBATCH --ntasks=XXXmpinodesXXX
#SBATCH --partition=XXXqueueXXX
#SBATCH --cpus-per-task=XXXthreadsXXX
#SBATCH --error=XXXerrfileXXX
#SBATCH --output=XXXoutfileXXX
#SBATCH --open-mode=append
#SBATCH --time=XXXextra1XXX
#SBATCH --mem-per-cpu=6G
##SBATCH XXXextra4XXX
##SBATCH XXXextra5XXX
##SBATCH XXXextra6XXX
env | sort
srun --mpi=pmi2 XXXcommandXXX
