create table richter(one varchar(10), two smallint, three vachar(50));
insert into richter values('Micro',1,'Cannot be felt');
insert into richter values('Minor',3,'Often felt');
insert into richter values('Moderate',5,'Felt. Light objects damaged');
insert into richter values('Major',7,'Heavy items damaged, large area');
insert into richter values('Massive',9,'Intense damage, buildings toppled, cracks in ground');
.save earthquake.db
.mode list
.separator |
.output textdump.txt
select * from richter;
.exit
